import {v4 as uuid} from 'uuid';

export interface AuthenticationResult {
    successful: boolean
}

export interface User {
    consent: boolean
    sessionID: string
}

export const USER_STORAGE_KEY = "user"

class UserAuthentication {
    DFUrl = "https://api.api.ai/v1/query?v=20160910&";
    backendAPIBaseUrl = "https://scrap.entty.eu:445/databums.php";
    accessToken = "696f15ce45db41a8bd844bb6fc1c937c";
    consent: boolean = false
    sessionID: string


    constructor() {
        try {
            let usr = localStorage.getItem(USER_STORAGE_KEY)
            if (usr) {
                let usrObj: User = JSON.parse(usr)
                this.consent = (usrObj.consent || false)
                this.sessionID = usrObj.sessionID
                console.log("is Agreed To Terms: " + this.consent)
            }
        } catch (ex) {
            this.consent = false
        }
    }

    agreeToConsent(): AuthenticationResult {
        this.consent = true;
        this.sessionID = uuid();
        localStorage.setItem(USER_STORAGE_KEY,
            JSON.stringify({
                consent: this.consent,
                sessionID: this.sessionID,
            })
        )
        return {"successful": true}
    }


}

export const authentication = new UserAuthentication()