import * as React from "react";
import {
    Button,
    Grid,
    GridColumn,
    GridRow,
    Image,
    Input,
    InputOnChangeData,
    List,
    ListDescription, Pagination,
} from "semantic-ui-react";
import {v4 as uuid} from 'uuid';
import {Feedback} from "../Feedback/Feedback";
import {IfBox} from "../style/if";
import {ChangeEvent} from "react";
import {googleResponse, sendToGoogle} from "../sendToGoogle";

export interface SearchBarProps {
    onFeedBackSubmit: () => void
}

export interface SearchBarState {
    openFeedbackModal: boolean,
    feedbackSubmitted: boolean,
    googlePage: number
    googleResponse?: googleResponse
    searchString: string
    feedbackSuccess: boolean
}

export class SearchBar extends React.Component<SearchBarProps, SearchBarState> {
    isFirstSearch: boolean = true
    constructor(props: SearchBarProps) {
        super(props)

        this.state = {
            googlePage: 1,
            googleResponse: undefined,
            openFeedbackModal: false,
            feedbackSubmitted: false,
            searchString: "",
            feedbackSuccess: false
        }
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.doSearch();
        }
    }

    async doSearch(fromPagination = false,next?: "next" | "prev") {
        let page = this.state.googlePage;
        // let isNewSearch: boolean = true;
        if (next && next === "next") {
            page++;
            // isNewSearch = false
        } else if (next === "prev") {
            page--;
            // isNewSearch = false
        } else {
            if (!this.state.googleResponse) {
                // isNewSearch = false
            }
        }
        // if(fromPagination){
        //     isNewSearch = false;
        // }
        let showFeedback: boolean = true;
        if(this.isFirstSearch || this.state.feedbackSubmitted ){
            showFeedback = false
        }
        const googleResponse: googleResponse = await sendToGoogle(this.state.searchString, page);
        this.setState({
            openFeedbackModal: showFeedback,
            feedbackSubmitted: false,
            googleResponse: googleResponse,
            googlePage: page
        },()=>{
            this.isFirstSearch = false
        });
    }

    render() {
        // return <div className="gcse-searchresults-only"/>
        return <Grid className={"full-height overFlowAuto"}>
            <GridRow>
                <GridColumn width={15}>
                    <Input placeholder='Search...' className={"full-width"}
                           onKeyPress={this.handleKeyPress}
                           onChange={(event: ChangeEvent, data: InputOnChangeData) => this.setState({searchString: data.value})}/>
                </GridColumn>
                <GridColumn width={1}>
                    <Button primary icon={"search"} onClick={() => this.doSearch()}
                            disabled={!this.state.searchString}/>
                </GridColumn>
            </GridRow>
            <GridRow>
                <GridColumn style={{overflow: "auto"}} className={"full-height"}>
                    <List animated>
                        {this.renderResults()}
                    </List>
                    <IfBox shouldShow={this.state.openFeedbackModal}>
                        <Feedback openModal={this.state.openFeedbackModal} onSubmit={this.onFeedbackSubmit}/>
                    </IfBox>
                </GridColumn>
            </GridRow>
            <IfBox shouldShow={this.state.googleResponse?.results && this.state.googleResponse?.results.length > 0}>
                <GridRow columns={3}>
                    <GridColumn>
                        <IfBox shouldShow={this.state.googleResponse?.hasPrev}>
                            <Button positive icon='left arrow' labelPosition='left' content="Previous"
                                    onClick={() => {
                                        this.doSearch(false,"prev")
                                    }}/>
                        </IfBox>
                    </GridColumn>
                    <GridColumn textAlign={"center"}>
                        <Pagination
                            defaultActivePage={1}
                            firstItem={null}
                            lastItem={null}
                            activePage={this.state.googlePage}
                            onPageChange={(e, {activePage}) => {
                                this.setState({googlePage: activePage as number},()=>{this.doSearch(true)})
                            }}
                            pointing
                            secondary
                            totalPages={10}
                        />
                    </GridColumn>
                    <GridColumn textAlign={"right"}>
                        <IfBox shouldShow={this.state.googleResponse?.hasNext && this.state.googlePage<10}>
                            <Button positive icon='right arrow' labelPosition='right' content="Next"
                                    onClick={() => {
                                        this.doSearch(false,"next")
                                    }}/>
                        </IfBox>
                    </GridColumn>
                </GridRow>
            </IfBox>
        </Grid>
    }

    onFeedbackSubmit = () => {
        this.setState({openFeedbackModal: false, feedbackSubmitted: true})
        this.props.onFeedBackSubmit()
    }

    renderResults() {
        return this.state.googleResponse?.results?.map((res) => {
            let img = ""
            if (res && res.pagemap && res.pagemap?.cse_image && res.pagemap?.cse_image[0] && res.pagemap?.cse_image[0]?.src)
                img = res.pagemap?.cse_image[0]?.src;
            return <List.Item key={uuid()}>
                <List.Content>
                    <List.Header>
                        <a href={res.formattedUrl} target="_blank" rel="noopener noreferrer"
                           onClick={() => {
                               this.setState({openFeedbackModal: !this.state.feedbackSubmitted})
                           }}>
                            <h3>{res.title}</h3>
                        </a>
                        <a href={res.formattedUrl} target="_blank" rel="noopener noreferrer"
                           onClick={() => {
                               this.setState({openFeedbackModal: !this.state.feedbackSubmitted})
                           }}>
                            {res.formattedUrl}
                        </a>
                    </List.Header>
                    <ListDescription>
                        <div>{img !== "" ? <Image src={img} size='tiny' verticalAlign='top'/> : null}</div>
                        <div>{res.snippet}</div>
                    </ListDescription>
                </List.Content>
            </List.Item>

        })
    }
}