import * as React from 'react';
import { Image } from 'semantic-ui-react';
import './box.css';
import logo from '../../logo.png';

type LogoProps = {
    showLogo?: boolean
}

export class Logo extends React.Component<LogoProps, any> {

    static defaultProps = {
        showLogo: false,
    }

    render() {
        return <div><Image src={logo} size={"tiny"} fluid className="image-large-padding"/></div>
    }
}

export default Logo;