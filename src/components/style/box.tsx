import * as React from 'react';
import { Grid } from 'semantic-ui-react';
import './box.css';
import Logo from "./logo";

type BoxProps = {
    showLogo?: boolean
}

export class Box extends React.Component<BoxProps, any> {

    static defaultProps = {
        showLogo: false,
    }

    constructor(props: BoxProps) {
        super(props)

        this.state = {
            activeIndex: 0,
            isLoading: false
        }
    }
    render() {

        return <div><Grid centered>
            <Grid.Row centered>
                <Grid.Column className="column-box">
                    {this.props.showLogo ? <Logo/> : null}
                    {this.props.children}
                </Grid.Column>
            </Grid.Row>
        </Grid></div>
    }
}

export default Box;