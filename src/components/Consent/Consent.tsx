import * as React from "react";
import {Button, Checkbox, Icon, Message, Segment} from 'semantic-ui-react';
import {authentication} from '../../authentication';
import {Route} from 'react-router-dom'


export interface ConsentState {
    consent: boolean,
}

export class Consent extends React.Component<any, ConsentState> {
    firstInteraction = false;

    constructor(props: any) {
        super(props)

        this.state = {
            consent: false
        }
    }

    renderMessage() {
        if (this.firstInteraction && !this.state.consent) {
            return (<Message attached='bottom' error>
                <Icon name='exclamation triangle'/>You have to agree to the terms and conditions.
            </Message>)
        }
        return (<div/>)
    }

    render() {
        return <Segment>
            <Message>
                <div className="pageBreak">
                    <h1>Informed Consent of Participation</h1>
                    <p>You are invited to participate in the online study Chatbot To Help initiated and conducted by
                        Juan Camilo Giraldo, Abdullah Khan Tarar, Daniel von Barany and Valentin Schwind. The research is
                        supervised by at the University of Applied Sciences in Frankfurt. Please note:</p>
                    <ul>
                        <li>Your participation is entirely voluntary</li>
                        <li>The online study will last approximately 10 minutes</li>
                        <li>We will record personal demographics (age, gender, etc.)</li>
                        <li>We may publish our results from this and other sessions in our reports, but all such reports
                            will be confidential and will neither include your name nor cannot be associated with your
                            identity
                        </li>
                    </ul>
                    <p>If you have any questions or complaints about the whole informed consent process of
                        this
                        research study or your rights as a human research subject, please contact our ethical committee
                        office: DFG/BMBF/Uni-Regensburg. You should carefully read the information below. Please
                        take as much time as you need to read the consent form.</p></div>
                <div className="pageBreak">
                    <h3>1. Purpose and Goal of this Research</h3>
                    <p>Chatbot with predictive model is used in a search engine to optimize search results. Improve
                        users' internet search efficiency Your participation will help us achieve this goal. The results
                        of this research may be presented at scientific or professional meetings or published in
                        scientific proceedings and journals.</p></div>
                <div className="pageBreak">
                    <h3>2. Participation and Compensation</h3>
                    <p>Your participation in this online study is completely voluntary. You will be one of approximately
                        100 people being surveyed for this research. You will receive no compensation. You may withdraw
                        and discontinue participation at any time without penalty. If you decline to participate or
                        withdraw from the online study, no one on the campus will be told. You may refuse to answer any
                        questions you do not want to answer.</p></div>
                <div className="pageBreak">
                    <h3>3. Procedure</h3>After confirming your informed consent you will:
                    <div style={{paddingLeft: "10px", paddingTop: "0px"}}>
                        1. Users open the search engine<br/>
                        2. Users will search the internet. <br/>
                        3. They check that the search if it narrows down to what they need. <br/>
                        4. Fill out a satisfaction survey that will be for academic use
                        The complete procedure of this online study will last approximately 10 minutes.
                    </div>
                    <p>The complete procedure of this user study will last approximately 60 minutes.</p>
                </div>
                <div className="pageBreak">
                    <h3>4. Risks and Benefits</h3>
                    <p>There are no risks associated with this online study. Discomforts or inconveniences will be minor
                        and are not likely to happen. If any discomforts become a problem, you may discontinue your
                        participation. You will not directly benefit through participation in this online study. We hope
                        that the information obtained from your participation may help to bring forward the research in
                        this field. </p>
                </div>
                <div className="pageBreak">
                    <h3>5. Data Protection and Confidentiality</h3>
                    <p>Personal data (age, gender, etc.) will be recorded while participation. The researcher will not
                        identify you by your real name in any reports using information obtained from this online study
                        and that your confidentiality as a participant in this online study will remain secure and
                        encrypted. All data you provide in this online study will be published anonymized and treated
                        confidentially in compliance with the General Data Protection Regulation (GDPR) of the European
                        Union (EU). In all cases, uses of records and data will be subject to the GDPR. Faculty and
                        administrators from the campus will not have access to raw data or transcripts. This precaution
                        will prevent your individual comments from having any negative repercussions. This site uses
                        cookies and other tracking technologies to conduct the research, to improve the user experience,
                        the ability to interact with the system and to provide additional content from third parties.
                        Despite careful control of content, the researchers assume no liability for damages, which
                        directly or indirectly result from the use of this online application. Any recordings cannot be
                        viewed by anyone outside this research project unless we have you sign a separate permission
                        form allowing us to use them (see below). The records will be destroyed after the end of the
                        research as required by the funding organization or if you contact the researcher to destroy or
                        delete them immediately. As with any publication or online related activity, the risk of a
                        breach of confidentiality is always possible. According to the GDPR, the researchers will inform
                        the participant if a breach of confidential data was detected. </p>
                </div>
                <div className="pageBreak">
                    <h3>6. Identification of Investigators</h3>
                    <p>If you have any questions or concerns about the research, please feel free to contact:</p>
                    <div className="row">
                        <div className="col-3 ml-3  mb-3"> Juan Camilo Giraldo, Abdullah Khan Tarar, Daniel von Barany<br/>
                            Student<br/>
                            juan.giraldosalazar@stud.fra-uas.de, abdullah.tarar@stud.fra-uas.de, daniel.vonbarany@stud.fra-uas.de
                        </div>
                        <div className="col-4 ml-3  mb-3">
                            Prof. Dr. Valentin Schwind<br/>
                            Principal Investigator<br/>
                            Human-Computer Interaction<br/>
                            Nibelungenplatz 1<br/>
                            60318 Frankfurt a. M., Germany<br/>
                            valentin.schwind@fb2.fra-uas.de
                        </div>
                        <br/>
                    </div>
                </div>
                <div className="pageBreak"><h3>7. Informed Consent and Agreement</h3>
                    {this.renderMessage()}
                    <Checkbox checked={this.state.consent} onChange={this.toggleCheckBox} label="I understand the explanation
                        provided to me. I understand the hygiene rules of the institution and I accept to follow them.
                        I have been given a copy of this form. I have had all my questions answered to my satisfaction,
                        and I voluntarily agree to participate in this user study."/>
                </div>
                <div className="pageBreak">
                    <Route render={({history}) => (
                        <Button primary onClick={() => {
                            this.submit(history)
                        }} disabled={!this.state.consent}>Submit</Button>
                    )}/>
                </div>
            </Message>

        </Segment>
    }

    toggleCheckBox = () => {
        this.firstInteraction = true;
        this.setState({consent: !this.state.consent})
    }

    submit(history) {
        authentication.agreeToConsent()
        console.info("Auth successful")
        history.push('/search')
    }
}