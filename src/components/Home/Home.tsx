import * as React from "react";
import {Container, Message, Rail, Segment} from "semantic-ui-react";
import {SearchBar} from "../SearchBar/SearchBar";
import {IfBox} from "../style/if";
import {Bot} from "../Bot/Bot";
import {Instructions} from "../instructions/instructions";

export class Home extends React.Component<any, any> {


    constructor(props: any) {
        super(props)
        this.state = {
            searchString: undefined,
            searchStringForBot: undefined,
            feedbackSuccess: false,
            showInstructions: true,
            showFieldOnly: this.getRandomInt(0, 1),
            // showFieldOnly: 0,
        }
    }

    componentDidMount() {
        const script = document.createElement("script");
        script.async = true;
        script.src = "https://cse.google.com/cse.js?cx=007175631489998748943:w29zkedhcri";
        document.head.appendChild(script);
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    render() {
        return <Container>
            {this.state.feedbackSuccess ?
                <Rail internal position='right' style={{zIndex: '999', top: '13px', right: '-42px'}}>
                    <Message positive>
                        <Message.Header>Thank you for your feedback.</Message.Header>
                    </Message>
                </Rail>
                : null}
            <Segment style={{top: "20px", height: "90vh", overflow: "auto"}}>
                {/*<GridRow>*/}
                {/*    <GridColumn>*/}
                {/*        <div className="gcse-searchbox-only"/>*/}
                {/*    </GridColumn>*/}
                {/*</GridRow>*/}
                <IfBox shouldShow={this.state.showFieldOnly}>
                    <SearchBar onFeedBackSubmit={this.onFeedBackSubmit}/>
                </IfBox>
                <IfBox shouldShow={!this.state.showFieldOnly}>
                    <Bot onFeedBackSubmit={this.onFeedBackSubmit}/>
                </IfBox>
            </Segment>
            <IfBox shouldShow={this.state.showInstructions}>
                <Instructions onClose={() => {this.setState({showInstructions: false})}} use_bot={!this.state.showFieldOnly}/>
            </IfBox>
        </Container>
    }

    onFeedBackSubmit = () => {
        this.setState({feedbackSuccess: true}, () => {
            setTimeout(() => {
                this.setState({feedbackSuccess: false})
            }, 5000);
        })
    }
}