import * as React from "react";
import {ChangeEvent, RefObject} from "react";
import {
    Divider, Grid, GridColumn, GridRow,
    Icon,
    Image,
    Input,
    InputOnChangeData,
    List,
    Message,
    SemanticCOLORS
} from "semantic-ui-react";
import './Bot.css';
import {v4 as uuid} from "uuid";
import userIcon from "../Bot/img/userIcon.png";
import bot from "../Bot/img/bot2.svg";
import {authentication} from "../../authentication";
import {Feedback} from "../Feedback/Feedback";
import {IfBox} from "../style/if";
import {googleResponse, sendToGoogle} from "../sendToGoogle";

export interface OnlyResultsProps {
    onFeedBackSubmit: () => void
}

export type msgPool = {
    msg: string,
    jsx?: JSX.Element
    isUserMsg: boolean,
    isSearch: boolean,
    searchTerm?: string
}

export interface OnlyResultsState {
    newMessage: string,
    prvSearch: string,
    inputValue: string,
    openFeedbackModal: boolean,
    feedbackSubmitted: boolean,
    checkResponse: boolean,
    searchResults: any,
    msgPool: msgPool[],
    showMsgListItems: boolean,
    googlePage: number,
}

export class Bot extends React.Component<OnlyResultsProps, OnlyResultsState> {
    afterResultMsg = <span>Please Type or click:
        <a href="#" onClick={() => {
            this.replyToUserResponseMsg("s")
        }}>[S]atisfied</a> => No need for more results,
        <a href="#" onClick={() => {
            this.replyToUserResponseMsg("n")
        }}>[N]ext</a> => Get more results</span>
    responseMsg = "Great! Can I help you with something else?";
    msgInputRef
    lastMsgIndex = 0
    messagesEndRef: RefObject<HTMLDivElement> = React.createRef();

    constructor(props: any) {
        super(props)

        this.state = {
            newMessage: "",
            inputValue: "",
            prvSearch: "",
            openFeedbackModal: false,
            feedbackSubmitted: false,
            checkResponse: false,
            searchResults: {},
            msgPool: [],
            showMsgListItems: false,
            googlePage: 1,
        }
        this.sendToDF("hi")
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<any>, snapshot?: any) {
        this.scrollToBottom();
    }

    onKeyDown = (keyName: any, e: any) => {
        if (e)
            e.preventDefault()
        if (keyName.key === "Enter" && keyName.keyCode === 13) {
            this.sendBtnClick();
        }
        if (keyName.key === "ArrowUp" && keyName.keyCode === 38) {
            let msgpool = JSON.parse(JSON.stringify(this.state.msgPool));
            let lastMsg = msgpool.reverse().find((m, i) => {
                if (m.isUserMsg && i > this.lastMsgIndex) {
                    this.lastMsgIndex = i;
                    return m
                }
            })
            this.setState({newMessage: lastMsg ? lastMsg.msg : ""})
        }
    }

    render() {
        let msgListItems = []

        if (this.state.msgPool.length > 0) {
            this.state.msgPool.forEach((msg: msgPool) => {
                msgListItems.push(
                    <List.Item key={uuid()}>
                        <List.Content floated={msg.isUserMsg ? 'left' : 'right'}>
                            {msg.isUserMsg ?
                                <Image src={userIcon} avatar fluid style={{display: "inline-block"}}/> : null}

                            <Message color={msg.isUserMsg ? 'black' as SemanticCOLORS : 'teal' as SemanticCOLORS}
                                     className={"msgBox"}>
                                {(msg.isSearch) ? <div>
                                        <Image src={bot} avatar fluid style={{display: "inline-block"}}
                                               onerror="this.style.display='none'"/>
                                        <div style={{display: "inline-block"}}>
                                            <h5>Search Results for term : {msg.searchTerm}</h5>
                                        </div>
                                        <ul style={{
                                            maxHeight: '350px',
                                            overflow: "auto"
                                        }}>{this.renderSearchResults(this.state.searchResults[msg.msg])}</ul>
                                    </div>
                                    : (msg.jsx) ? msg.jsx : <div dangerouslySetInnerHTML={{__html: msg.msg}}/>}
                            </Message>

                            {!msg.isUserMsg && !msg.isSearch ?
                                <Image src={bot} avatar fluid style={{display: "inline-block"}}/> : null}
                        </List.Content>
                    </List.Item>
                )
            })
        }
        return <Grid className={"full-height overFlowAuto"}>
            <GridRow>
                <GridColumn style={{overflow: "auto"}} className={"full-height"}>
                    <div className="chat-bot-container full-height">
                        <div className={"chat-msg-container"}>
                            <List>
                                {/*<List.Item key={uuid()}>*/}
                                {/*    <List.Content>*/}
                                {/*        <Message className={"msgBox"}>*/}
                                {/*            <div>Please use "bot_search [Search term]" to use searching feature of the*/}
                                {/*                bot.*/}
                                {/*            </div>*/}
                                {/*        </Message>*/}
                                {/*    </List.Content>*/}
                                {/*</List.Item>*/}
                                {msgListItems}
                                <div style={{float: "left", clear: "both"}} ref={this.messagesEndRef}/>
                            </List>
                        </div>
                        <div className={"chat-input-container"}>
                            <Input icon={<Icon name='send' inverted circular link onClick={this.sendBtnClick}/>}
                                   onKeyDown={this.onKeyDown} placeholder='Press Enter to send...'
                                   className={"chat-input"}
                                   value={this.state.newMessage}
                                   ref={(input) => {
                                       this.msgInputRef = input;
                                   }}
                                   onChange={(event: ChangeEvent, data: InputOnChangeData) => this.setState({newMessage: data.value})}/>
                        </div>
                        <IfBox shouldShow={this.state.openFeedbackModal}>
                            <Feedback openModal={this.state.openFeedbackModal} use_bot
                                      onSubmit={this.onFeedBackSubmit}/>
                        </IfBox>
                    </div>
                </GridColumn>
            </GridRow>
        </Grid>
    }

    sendBtnClick = () => {
        if (this.state.newMessage !== "" || this.state.newMessage) {
            const msgPool = this.state.msgPool;
            const newMsg = this.state.newMessage;
            msgPool.push({msg: newMsg, isUserMsg: true, isSearch: false})
            this.lastMsgIndex = 0;
            this.setState({msgPool: msgPool, newMessage: ""},
                () => {
                    this.msgInputRef.focus();
                    this.msgInputRef.inputRef.current.value = "";
                    if (this.state.checkResponse) {
                        this.replyToUserResponseMsg(newMsg)
                    } else if (newMsg.includes("bot_search")) {
                        this.doSearch(newMsg);
                    } else {
                        this.sendToDF(newMsg);
                    }
                })
        }
    }

    //------------------------------------------- Send request to API.AI ---------------------------------------
    sendToDF(text) {
        fetch(authentication.DFUrl + "query=" + text + "&lang=en-us&sessionId=" + authentication.sessionID, {
            method: "GET",
            headers: {
                "Authorization": "Bearer " + authentication.accessToken
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.processDFResult(result)
                },
                (error) => {
                    console.log("DialogueFlow ERROR: ", error)
                }
            )
    }

    processDFResult(data) {
        let action = data.result.action;
        let searchParameter = data?.result?.parameters?.any;
        let intentName = data?.result?.metadata?.intentName;
        let speech = data.result.fulfillment.speech;
        let suggestions;
        // use incomplete if u use required in api.ai questions in intent
        // check if actionIncomplete = false
        // var incomplete = data.result.actionIncomplete;
        if (data.result.fulfillment.messages) { // check if messages are there
            if (data.result.fulfillment.messages.length > 0) { //check if quick replies are there
                suggestions = data.result.fulfillment.messages[1];
            }
        }
        if (intentName === "bot_search" && !searchParameter) {
            const msgPool = this.state.msgPool;
            msgPool.push({msg: "Sorry I didn't get that.", isUserMsg: false, isSearch: false})
            this.setState({msgPool: msgPool, newMessage: ""},
                () => {
                    this.msgInputRef.focus();
                })
        } else if (speech) {

            const msgPool = this.state.msgPool;
            if (speech.includes('bot_search')) {
                this.doSearch(speech.split("bot_search")[1].trim(), false)
            } else {
                msgPool.push({msg: speech, isUserMsg: false, isSearch: false})
                this.setState({msgPool: msgPool, newMessage: ""},
                    () => {
                        this.msgInputRef.focus();
                    })
            }
        }
    }

    onFeedBackSubmit = () => {
        this.setState({openFeedbackModal: false, feedbackSubmitted: true})
        this.props.onFeedBackSubmit()
    }

    async doSearch(searchQuery, trim = true) {
        const searchTerm = (trim) ? searchQuery.split("bot_search")[1].trim() : searchQuery;
        if (!searchTerm || searchTerm === "") {
            return
        }
        const googleResponse: googleResponse = await sendToGoogle(searchTerm, this.state.googlePage);
        let results = googleResponse.results;
        // let wikiresults = await sendToWikipedia(searchTerm)
        if (results && results.length > 0) {
            let searchResultsID = uuid();
            let searchResults = this.state.searchResults;
            searchResults[searchResultsID] = results;
            const msgPool = this.state.msgPool;
            msgPool.push({msg: searchResultsID, isUserMsg: false, isSearch: true, searchTerm: searchTerm})
            msgPool.push({msg: "", jsx: this.afterResultMsg, isUserMsg: false, isSearch: false, searchTerm: ""})
            this.setState({
                    msgPool: msgPool,
                    searchResults: searchResults,
                    checkResponse: true,
                    prvSearch: searchQuery,
                    feedbackSubmitted: false
                },
                () => {
                    this.msgInputRef.focus();
                    this.msgInputRef.inputRef.current.value = "";
                })
            this.scrollToBottom();
        }
    }

    private renderSearchResults(results) {
        return results?.map((res) => {
            let img = ""
            if (res && res.pagemap && res.pagemap?.cse_image && res.pagemap?.cse_image[0] && res.pagemap?.cse_image[0]?.src)
                img = res.pagemap?.cse_image[0]?.src;
            return <li key={uuid()} style={{width: "97%"}}>
                <div><a href={res.formattedUrl} target="_blank" rel="noopener noreferrer"
                        onClick={() => this.setState({openFeedbackModal: !this.state.feedbackSubmitted})}>
                    <h3>{res.title}</h3>
                </a>
                    <a href={res.formattedUrl} target="_blank" rel="noopener noreferrer"
                       onClick={() => this.setState({openFeedbackModal: !this.state.feedbackSubmitted})}>
                        {res.formattedUrl}
                    </a>
                </div>
                <Grid>
                    <GridRow columns={2}>
                        <GridColumn width={2}>
                            <div style={{display: "inline-block"}}>{img !== "" ?
                                <Image src={img} size='tiny' verticalAlign='top'/> : null}</div>
                        </GridColumn>
                        <GridColumn width={14}>
                            <div style={{display: "inline-block"}}>{res.snippet}</div>
                        </GridColumn>
                    </GridRow>
                </Grid>
                <Divider/>
            </li>
        })
    }

    private replyToUserResponseMsg(newMsg: string) {
        if (this.state.checkResponse) {
            const msg = newMsg.toLowerCase();
            const s = (msg === 'satisfied' || msg === '[s]atisfied' || msg === '[s]' || msg === 's')
            const n = (msg === 'next' || msg === '[n]ext' || msg === '[n]' || msg === 'n')
            const msgPool = this.state.msgPool;
            let showFeedbackForm = false;
            let checkResponse = JSON.parse(JSON.stringify(this.state.checkResponse));
            if (s) {
                msgPool.push({msg: this.responseMsg, isUserMsg: false, isSearch: false})
                showFeedbackForm = !this.state.feedbackSubmitted;
                checkResponse = false;
            } else if (n) {
                this.setState({googlePage: this.state.googlePage + 1}, () => {
                    this.doSearch(this.state.prvSearch, false)
                })
                return
            } else {
                msgPool.push({msg: "", jsx: this.afterResultMsg, isUserMsg: false, isSearch: false})
                checkResponse = true;
            }
            this.setState({
                msgPool: msgPool,
                newMessage: "",
                checkResponse: checkResponse,
                openFeedbackModal: showFeedbackForm
            })
        }
    }

    scrollToBottom = () => {
        this.messagesEndRef.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    }
}