const GoogleID = "007175631489998748943:w29zkedhcri";
const GoogleAPIKeys = ["AIzaSyBbtU8iAGoYVhWtv4irXn28W5CcJIEZzB8", "AIzaSyBargkxN0DzdQkKauNkk_mj9YUMIFpdJPA", "AIzaSyBWMARKVESPTzyyuZzMWIMoaIhwUKhO3lk"];
let GoogleAPIKeyIndex = 0;

export interface googleResponse {
    results?: any[],
    searchInformation?: googleSearchStats,
    hasNext?: boolean,
    hasPrev?: boolean
}

export interface googleSearchStats {
    formattedSearchTime?: string
    formattedTotalResults?: string
    searchTime?: number
    totalResults?: number
}

export async function sendToGoogle(searchQuery: string, page: number = 1): Promise<any> {
    let searchResponse: googleResponse = {}
    const startResultsFrom = (page * 10) - 9

    let loop = true;
    if (searchQuery) {
        while (loop) {
            await fetch("https://www.googleapis.com/customsearch/v1?key=" + GoogleAPIKeys[GoogleAPIKeyIndex]
                + "&cx=" + GoogleID
                + "&q=" + searchQuery
                + "&start=" + startResultsFrom)
                .then(response => response.json())
                .then(response => {
                    if (response.error) {
                        console.log("GoogleAPI Error: ", response.error)
                        if (GoogleAPIKeyIndex === GoogleAPIKeys.length) {
                            console.log("We are out of API keys");
                            loop = false;
                        }
                        if (GoogleAPIKeyIndex < GoogleAPIKeys.length) {
                            GoogleAPIKeyIndex++;
                        }
                    }else{
                        if (response.searchInformation) {
                            loop = false;
                        }
                        searchResponse = {
                            results: response.items,
                            searchInformation: response.searchInformation,
                            hasNext: !!response.queries.nextPage,
                            hasPrev: !!response.queries.previousPage
                        };
                    }
                });
        }
    }

    return searchResponse;
}