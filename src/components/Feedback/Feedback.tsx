import * as React from "react";
import {Button, Message, Modal, Rating, Segment} from 'semantic-ui-react';
import {authentication} from "../../authentication";

export interface FeedbackProps {
    openModal: boolean,
    use_bot?: boolean,
    onSubmit: () => void,
}

export interface FeedbackState {
    answers: {
        q1: boolean | null,
        q2: number,
        q3: number,
        q4: number,
        q5: number,
        q6: number,
    },
    questions: {
        header: string,
        1: string,
        2: string,
        3: string,
        4: string,
        5: string,
        6: string
    }
    showError: boolean,
}


export class Feedback extends React.Component<FeedbackProps, FeedbackState> {
    use_bot = false;

    constructor(props: any) {
        super(props)
        if (this.props.use_bot) {
            this.use_bot = this.props.use_bot;
        }
        this.state = {
            answers: {
                q1: null,
                q2: 0,
                q3: 0,
                q4: 0,
                q5: 0,
                q6: 0,
            },
            questions: {
                header: "",
                1: "",
                2: "",
                3: "",
                4: "",
                5: "",
                6: ""
            },
            showError: false
        }
        this.getQuestions()
    }

    render() {
        return <Modal dimmer open
                      closeOnDimmerClick={false}
                      closeOnEscape={false}>
            <Modal.Header>Feedback</Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    <Segment>
                        {(this.state.showError && this.state.answers.q1 === null) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[1]}</p>
                        <Button style={{marginBottom: '5px'}} primary={this.state.answers.q1} onClick={()=>this.handleRate("q1",1)}> YES</Button>
                        {/*<Radio toggle checked={this.state.answers.q1} onChange={(event: React.FormEvent<HTMLInputElement>, data: CheckboxProps)=>*/}
                        {/*this.handleRate('q1', data.value)}/>*/}
                        <Button style={{marginBottom: '5px'}} secondary={this.state.answers.q1 == false} onClick={()=>this.handleRate("q1",0)}> NO</Button>
                    </Segment>
                    <div><b>{this.state.questions?.header}</b></div>
                    <Segment>
                        {(this.state.showError && this.state.answers.q2 === 0) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[2]}</p>
                        <Rating maxRating={7} onRate={(e, {rating}) => this.handleRate('q2', rating)}/>
                    </Segment>
                    <Segment>
                        {(this.state.showError && this.state.answers.q3 === 0) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[3]}</p>
                        <Rating maxRating={7} onRate={(e, {rating}) => this.handleRate('q3', rating)}/>
                    </Segment>
                    <Segment>
                        {(this.state.showError && this.state.answers.q4 === 0) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[4]}</p>
                        <Rating maxRating={7} onRate={(e, {rating}) => this.handleRate('q4', rating)}/>
                    </Segment>
                    <Segment>
                        {(this.state.showError && this.state.answers.q5 === 0) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[5]}</p>
                        <Rating maxRating={7} onRate={(e, {rating}) => this.handleRate('q5', rating)}/>
                    </Segment>
                    <Segment>
                        {(this.state.showError && this.state.answers.q6 === 0) ?
                            <Message negative> Please provide an answer. </Message>
                            : null}
                        <p> {this.state.questions[6]}</p>
                        <Rating maxRating={7} onRate={(e, {rating}) => this.handleRate('q6', rating)}/>
                    </Segment>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button positive icon='checkmark' labelPosition='right' content="Submit" onClick={this.submit}/>
            </Modal.Actions>
        </Modal>
    }

    handleRate = (questionNumber, rating) => {
        let answers = this.state.answers;
        if(questionNumber === '1'){

        } else {
            answers[questionNumber] = rating
        }
        this.setState({answers: answers})
    }

    submit = () => {
        console.log("OnSubmit")
        if (this.state.answers) {
            if (this.state.answers.q1 === null || this.state.answers.q2 === 0 || this.state.answers.q3 === 0
                || this.state.answers.q4 === 0 || this.state.answers.q5 === 0 || this.state.answers.q6 === 0) {
                console.log("please answer all the questions")
                this.setState({showError: true})
                return;
            } else {
                this.setState({showError: false})
            }
            let data = {
                uid: authentication.sessionID,
                q1: this.state.answers.q1 ? 1 : 0,
                q2: this.state.answers.q2,
                q3: this.state.answers.q3,
                q4: this.state.answers.q4,
                q5: this.state.answers.q5,
                q6: this.state.answers.q6,
                use_bot: this.use_bot ? 1 : 0
            }
            fetch(authentication.backendAPIBaseUrl, {
                method: "POST",
                body: JSON.stringify(data)
            }).then(res => {
                console.log("Request complete! response:", res);
            });
            this.props.onSubmit();
        }
    }

    async getQuestions() {
        await fetch("https://scrap.entty.eu:445/questions.json", {
            method: "GET",
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("Questions", result)
                    this.setState({questions:result});
                },
                (error) => {
                    console.log("ERRORR", error)
                }
            )
    }
}